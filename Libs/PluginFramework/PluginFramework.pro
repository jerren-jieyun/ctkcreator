QT += core sql concurrent
TEMPLATE = lib
DEFINES += PLUGINFRAMEWORK_LIBRARY
TARGET = PluginFramework

INCLUDEPATH += $$PWD/../Core

CONFIG(debug, debug|release) {
LIBS += -L$$PWD/../../bin/qmake/debug -lCore
DESTDIR = $$PWD/../../bin/qmake/debug
} else {
LIBS += -L$$PWD/../../bin/qmake/release -lCore
DESTDIR = $$PWD/../../bin/qmake/release
}

HEADERS += \
    ctkApplicationRunnable.h \
    ctkBasicLocation_p.h \
    ctkDefaultApplicationLauncher_p.h \
    ctkDictionary.h \
    ctkLDAPExpr_p.h \
    ctkLDAPSearchFilter.h \
    ctkLocationManager_p.h \
    ctkPlugin.h \
    ctkPlugin_p.h \
    ctkPluginAbstractTracked_p.h \
    ctkPluginActivator.h \
    ctkPluginArchive_p.h \
    ctkPluginArchiveSQL_p.h \
    ctkPluginConstants.h \
    ctkPluginContext.h \
    ctkPluginContext_p.h \
    ctkPluginDatabaseException.h \
    ctkPluginEvent.h \
    ctkPluginException.h \
    ctkPluginFramework.h \
    ctkPluginFramework_global.h \
    ctkPluginFramework_p.h \
    ctkPluginFrameworkContext_p.h \
    ctkPluginFrameworkDebug_p.h \
    ctkPluginFrameworkDebugOptions_p.h \
    ctkPluginFrameworkEvent.h \
    ctkPluginFrameworkFactory.h \
    ctkPluginFrameworkLauncher.h \
    ctkPluginFrameworkListeners_p.h \
    ctkPluginFrameworkProperties_p.h \
    ctkPluginFrameworkUtil_p.h \
    ctkPluginLocalization.h \
    ctkPluginManifest_p.h \
    ctkPlugins_p.h \
    ctkPluginStorage_p.h \
    ctkPluginStorageSQL_p.h \
    ctkPluginTracker.h \
    ctkPluginTracker_p.h \
    ctkPluginTrackerCustomizer.h \
    ctkRequirePlugin_p.h \
    ctkServiceEvent.h \
    ctkServiceException.h \
    ctkServiceFactory.h \
    ctkServiceProperties_p.h \
    ctkServiceReference.h \
    ctkServiceReference_p.h \
    ctkServiceRegistration.h \
    ctkServiceRegistration_p.h \
    ctkServices_p.h \
    ctkServiceSlotEntry_p.h \
    ctkServiceTracker.h \
    ctkServiceTracker_p.h \
    ctkServiceTrackerCustomizer.h \
    ctkTrackedPlugin_p.h \
    ctkTrackedPluginListener_p.h \
    ctkTrackedService_p.h \
    ctkTrackedServiceListener_p.h \
    ctkVersion.h \
    ctkVersionRange_p.h \
    service/application/ctkApplicationDescriptor.h \
    service/application/ctkApplicationException.h \
    service/application/ctkApplicationHandle.h \
    service/application/ctkApplicationLauncher.h \
    service/cm/ctkConfiguration.h \
    service/cm/ctkConfigurationAdmin.h \
    service/cm/ctkConfigurationEvent.h \
    service/cm/ctkConfigurationException.h \
    service/cm/ctkConfigurationListener.h \
    service/cm/ctkConfigurationPlugin.h \
    service/cm/ctkManagedService.h \
    service/cm/ctkManagedServiceFactory.h \
    service/datalocation/ctkLocation.h \
    service/debug/ctkDebugOptions.h \
    service/debug/ctkDebugOptionsListener.h \
    service/event/ctkEvent.h \
    service/event/ctkEventAdmin.h \
    service/event/ctkEventConstants.h \
    service/event/ctkEventHandler.h \
    service/log/ctkLogEntry.h \
    service/log/ctkLogListener.h \
    service/log/ctkLogReaderService.h \
    service/log/ctkLogService.h \
    service/log/ctkLogStream.h \
    service/metatype/ctkAttributeDefinition.h \
    service/metatype/ctkMetaTypeInformation.h \
    service/metatype/ctkMetaTypeProvider.h \
    service/metatype/ctkMetaTypeService.h \
    service/metatype/ctkObjectClassDefinition.h \
    ctkPluginFrameworkExport.h \
    ctkConfig.h \
    service/pluginadmin/pluginAdmin.h

SOURCES += \
    ctkApplicationRunnable.cpp \
    ctkBasicLocation.cpp \
    ctkDefaultApplicationLauncher.cpp \
    ctkLDAPExpr.cpp \
    ctkLDAPSearchFilter.cpp \
    ctkLocationManager.cpp \
    ctkPlugin.cpp \
    ctkPlugin_p.cpp \
    ctkPluginAbstractTracked.tpp \
    ctkPluginArchive.cpp \
    ctkPluginArchiveSQL.cpp \
    ctkPluginConstants.cpp \
    ctkPluginContext.cpp \
    ctkPluginDatabaseException.cpp \
    ctkPluginEvent.cpp \
    ctkPluginException.cpp \
    ctkPluginFramework.cpp \
    ctkPluginFramework_p.cpp \
    ctkPluginFrameworkContext.cpp \
    ctkPluginFrameworkDebug.cpp \
    ctkPluginFrameworkDebugOptions.cpp \
    ctkPluginFrameworkEvent.cpp \
    ctkPluginFrameworkFactory.cpp \
    ctkPluginFrameworkLauncher.cpp \
    ctkPluginFrameworkListeners.cpp \
    ctkPluginFrameworkProperties.cpp \
    ctkPluginFrameworkUtil.cpp \
    ctkPluginLocalization.cpp \
    ctkPluginManifest.cpp \
    ctkPlugins.cpp \
    ctkPluginStorageSQL.cpp \
    ctkPluginTracker.tpp \
    ctkPluginTracker_p.tpp \
    ctkRequirePlugin.cpp \
    ctkServiceEvent.cpp \
    ctkServiceException.cpp \
    ctkServiceProperties.cpp \
    ctkServiceReference.cpp \
    ctkServiceReference_p.cpp \
    ctkServiceRegistration.cpp \
    ctkServiceRegistration_p.cpp \
    ctkServices.cpp \
    ctkServiceSlotEntry.cpp \
    ctkServiceTracker.tpp \
    ctkServiceTracker_p.tpp \
    ctkTrackedPlugin.tpp \
    ctkTrackedService.tpp \
    ctkVersion.cpp \
    ctkVersionRange.cpp \
    service/application/ctkApplicationDescriptor.cpp \
    service/application/ctkApplicationException.cpp \
    service/application/ctkApplicationHandle.cpp \
    service/cm/ctkConfiguration.cpp \
    service/cm/ctkConfigurationAdmin.cpp \
    service/cm/ctkConfigurationEvent.cpp \
    service/cm/ctkConfigurationException.cpp \
    service/cm/ctkConfigurationPlugin.cpp \
    service/datalocation/ctkLocation.cpp \
    service/debug/ctkDebugOptions.cpp \
    service/event/ctkEvent.cpp \
    service/event/ctkEventConstants.cpp \
    service/log/ctkLogService.cpp \
    service/log/ctkLogStream.cpp \
    service/metatype/ctkAttributeDefinition.cpp \
    service/metatype/ctkMetaTypeProvider.cpp \
    service/metatype/ctkMetaTypeService.cpp
