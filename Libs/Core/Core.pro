QT += core
TEMPLATE = lib
DEFINES += PluginFramework_CORE_LIBRARY
TARGET = Core

CONFIG(debug, debug|release) {
DESTDIR = $$PWD/../../bin/qmake/debug
} else {
DESTDIR = $$PWD/../../bin/qmake/release
}

# 修复MSVC2013编译问题
win32:msvc: LIBS += -lDbghelp

HEADERS += \
    ctkAbstractFactory.h \
    ctkAbstractFileBasedFactory.h \
    ctkAbstractLibraryFactory.h \
    ctkAbstractObjectFactory.h \
    ctkAbstractPluginFactory.h \
    ctkAbstractQObjectFactory.h \
    ctkBackTrace.h \
#    ctkBinaryFileDescriptor.h \
    ctkBooleanMapper.h \
    ctkCallback.h \
    ctkCommandLineParser.h \
    ctkCompilerDetections_p.h \
    ctkCoreExport.h \
    ctkCoreTestingMacros.h \
    ctkCoreTestingUtilities.h \
    ctkDependencyGraph.h \
    ctkErrorLogAbstractMessageHandler.h \
    ctkErrorLogContext.h \
    ctkErrorLogFDMessageHandler.h \
    ctkErrorLogFDMessageHandler_p.h \
    ctkErrorLogLevel.h \
    ctkErrorLogQtMessageHandler.h \
    ctkErrorLogStreamMessageHandler.h \
    ctkErrorLogTerminalOutput.h \
    ctkException.h \
    ctkFileLogger.h \
    ctkHighPrecisionTimer.h \
    ctkLinearValueProxy.h \
    ctkLogger.h \
    ctkModelTester.h \
    ctkPimpl.h \
    ctkScopedCurrentDir.h \
    ctkSetName.h \
    ctkSingleton.h \
    ctkUtils.h \
    ctkValueProxy.h \
    ctkWorkflow.h \
    ctkWorkflow_p.h \
    ctkWorkflowStep.h \
    ctkWorkflowStep_p.h \
    ctkWorkflowTransitions.h

SOURCES += \
    ctkAbstractFactory.tpp \
    ctkAbstractFileBasedFactory.tpp \
    ctkAbstractLibraryFactory.tpp \
    ctkAbstractObjectFactory.tpp \
    ctkAbstractPluginFactory.tpp \
    ctkAbstractQObjectFactory.tpp \
    ctkBackTrace.cpp \
#    ctkBinaryFileDescriptor.cpp \
    ctkBooleanMapper.cpp \
    ctkCallback.cpp \
    ctkCommandLineParser.cpp \
    ctkCoreTestingUtilities.cpp \
    ctkCoreTestingUtilities.tpp \
    ctkDependencyGraph.cpp \
    ctkErrorLogAbstractMessageHandler.cpp \
    ctkErrorLogFDMessageHandler.cpp \
    ctkErrorLogLevel.cpp \
    ctkErrorLogQtMessageHandler.cpp \
    ctkErrorLogStreamMessageHandler.cpp \
    ctkErrorLogTerminalOutput.cpp \
    ctkException.cpp \
    ctkFileLogger.cpp \
    ctkHighPrecisionTimer.cpp \
    ctkLinearValueProxy.cpp \
    ctkLogger.cpp \
    ctkModelTester.cpp \
    ctkScopedCurrentDir.cpp \
    ctkSetName.cpp \
    ctkUtils.cpp \
    ctkValueProxy.cpp \
    ctkWorkflow.cpp \
    ctkWorkflowStep.cpp
