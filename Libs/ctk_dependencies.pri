CONFIG(debug, debug|release) {
LIBS *= -L$$PWD/../bin/qmake/debug -lCore -lPluginFramework
} else {
LIBS *= -L$$PWD/../bin/qmake/release -lCore -lPluginFramework
}

INCLUDEPATH *= $$PWD/Core \
               $$PWD/PluginFramework
