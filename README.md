# `ctkCreator`

 ## 介绍

`ctkCreator`是基于 `CTK Plugin Framework`插件式开发框架的一个源码集成开发环境， `CTK Plugin Framework` 基于` OSGI`（开放服务网关协议，`Open Service Gateway Initiative`）模块化编程的一个`c++/qt`实现版本。`ctkCreator`支持`qmake`与`cmake`两种方式源码集成，支持`windows`、`linux`、`ios`和安卓等平台。采用源码集成方案，方便用户跟踪内核框架代码流程，用利于用户快速了解内核框架原理。

![输入图片说明](doc/image/qmake_cmake.jpg ) 

`qmake`与`cmake`源码集成工程，目前只支持 debug 与 release 编译调试模式。

![输入图片说明](doc/image/debug.jpg) 

## 更新：

1.支持`qmake`和`cmake`源码集成`CTK Plugin Framework`开发。

2.编写消息订阅与发布模式的演示插件。

3.开发系统插件：插件管理器，用于插件的安装、启动、停止和卸载的管理。

## 教程

<a href="https://gitee.com/qtio/ctkcreator/blob/master/doc/introduce.md" target="_blank">1.CTK Plugin Framework 简介</a>

## 参与贡献

`侯磊`