CTKCREATOR_VERSION = 1.0.0

# 开启 c++11
CONFIG += c++11

defineTest(minQtVersion) {
    maj = $$1
    min = $$2
    patch = $$3
    isEqual(QT_MAJOR_VERSION, $$maj) {
        isEqual(QT_MINOR_VERSION, $$min) {
            isEqual(QT_PATCH_VERSION, $$patch) {
                return(true)
            }
            greaterThan(QT_PATCH_VERSION, $$patch) {
                return(true)
            }
        }
        greaterThan(QT_MINOR_VERSION, $$min) {
            return(true)
        }
    }
    greaterThan(QT_MAJOR_VERSION, $$maj) {
        return(true)
    }
    return(false)
}

# 项目目录
IDE_PATH        = $$PWD
IDE_APP_PATH    = $$IDE_PATH/qmake
IDE_LIB_PATH    = $$IDE_PATH/Libs
IDE_PLUGIN_PATH = $$IDE_PATH/plugins
IDE_SHARED_PATH = $$IDE_PATH/shared
IDE_TEST_PATH   = $$IDE_PATH/tests

# 项目生成目录
IDE_OUTPUT_PATH = $$IDE_PATH/bin

# 应用生成目录
CONFIG(debug, debug|release) {
    IDE_OUTPUT_APP_PATH    = $$IDE_OUTPUT_PATH/qmake/debug
    IDE_OUTPUT_PLUGIN_PATH = $$IDE_OUTPUT_PATH/qmake/debug/plugins
    IDE_OUTPUT_TEST_PATH   = $$IDE_OUTPUT_PATH/tests/debug
} else {
    IDE_OUTPUT_APP_PATH    = $$IDE_OUTPUT_PATH/qmake/release
    IDE_OUTPUT_PLUGIN_PATH = $$IDE_OUTPUT_PATH/qmake/release/plugins
    IDE_OUTPUT_TEST_PATH   = $$IDE_OUTPUT_PATH/qmake/tests/release
}

# 包含Qt模块
qt {
    contains(QT, core): QT += concurrent
    contains(QT, gui):  QT += widgets
}

# MSVC需加以下参数兼容XP
win32:msvc {
    QMAKE_LFLAGS_WINDOWS = /SUBSYSTEM:WINDOWS,5.01
    QMAKE_LFLAGS_CONSOLE = /SUBSYSTEM:CONSOLE,5.01
}

