include($$PWD/../../Plugins/Plugins.pri)

TARGET = pluginadmin

SOURCES += \
    pluginadminactivator.cpp \
    pluginadminthread.cpp \
    pluginadminservice.cpp \
    pluginadmindialog.cpp

HEADERS += \
    pluginadminactivator.h \
    pluginadminthread.h \
    pluginadminservice.h \
    pluginadmindialog.h

RESOURCES += \
    resource.qrc

FORMS += \
    dialog.ui

