TEMPLATE = subdirs
CONFIG  += ordered

SUBDIRS += \
    org.commontk.configadmin \
    org.commontk.eventadmin \
    org.commontk.metatype \
    org.commontk.log \
    cn.qtio.pluginadmin \
    cn.qtio.desktop \
    cn.qtio.subscriber \
    cn.qtio.publisher
