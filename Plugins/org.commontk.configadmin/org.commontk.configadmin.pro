include($$PWD/../../Plugins/Plugins.pri)
TARGET = configadmin
DEFINES += HAVE_QT5

HEADERS += \
    ctkCMEventDispatcher_p.h \
    ctkCMLogTracker_p.h \
    ctkCMPluginManager_p.h \
    ctkCMSerializedTaskQueue_p.h \
    ctkConfigurationAdminActivator_p.h \
    ctkConfigurationAdminFactory_p.h \
    ctkConfigurationAdminImpl_p.h \
    ctkConfigurationEventAdapter_p.h \
    ctkConfigurationImpl_p.h \
    ctkConfigurationStore_p.h \
    ctkManagedServiceFactoryTracker_p.h \
    ctkManagedServiceTracker_p.h

SOURCES += \
    ctkCMEventDispatcher.cpp \
    ctkCMLogTracker.cpp \
    ctkCMPluginManager.cpp \
    ctkCMSerializedTaskQueue.cpp \
    ctkConfigurationAdminActivator.cpp \
    ctkConfigurationAdminFactory.cpp \
    ctkConfigurationAdminImpl.cpp \
    ctkConfigurationEventAdapter.cpp \
    ctkConfigurationImpl.cpp \
    ctkConfigurationStore.cpp \
    ctkManagedServiceFactoryTracker.cpp \
    ctkManagedServiceTracker.cpp

RESOURCES += \
    resource.qrc
