include($$PWD/../../Plugins/Plugins.pri)

QT       += network

TARGET = desktop

SOURCES += \
    mainwindow.cpp \
    desktopactivator.cpp

HEADERS += \
    mainwindow.h \
    desktopactivator.h

RESOURCES += \
    resource.qrc

FORMS += \
    mainwindow.ui



