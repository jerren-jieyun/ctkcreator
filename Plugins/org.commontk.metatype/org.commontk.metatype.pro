#-------------------------------------------------
#
# Project created by QtCreator 2018-03-07T11:22:17
#
#-------------------------------------------------
include($$PWD/../../Plugins/Plugins.pri)
TARGET = metatype
DEFINES += HAVE_QT5

SOURCES += \
    ctkAttributeDefinitionImpl.cpp \
    ctkMetaTypeActivator.cpp \
    ctkMetaTypeInformationImpl.cpp \
    ctkMetaTypeProviderImpl.cpp \
    ctkMetaTypeServiceImpl.cpp \
    ctkMTDataParser.cpp \
    ctkMTIcon.cpp \
    ctkMTLocalizationElement.cpp \
    ctkMTLogTracker.cpp \
    ctkMTMsg.cpp \
    ctkMTProviderTracker.cpp \
    ctkObjectClassDefinitionImpl.cpp

HEADERS += \
    ctkAttributeDefinitionImpl_p.h \
    ctkMetaTypeActivator_p.h \
    ctkMetaTypeInformationImpl_p.h \
    ctkMetaTypeProviderImpl_p.h \
    ctkMetaTypeServiceImpl_p.h \
    ctkMTDataParser_p.h \
    ctkMTIcon_p.h \
    ctkMTLocalizationElement_p.h \
    ctkMTLogTracker_p.h \
    ctkMTMsg_p.h \
    ctkMTProviderTracker_p.h \
    ctkObjectClassDefinitionImpl_p.h

RESOURCES += \
    resource.qrc
