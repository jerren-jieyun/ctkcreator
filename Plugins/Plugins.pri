include(../CTK.pri)
include($$IDE_LIB_PATH/ctk_dependencies.pri)

TEMPLATE = lib
CONFIG += plugin

# 插件生成目录
DESTDIR += $$IDE_OUTPUT_PLUGIN_PATH

# 插件间依赖
LIBS += -L$$DESTDIR
INCLUDEPATH += $$IDE_PLUGIN_PATH

# 预编译头
isEmpty(PRECOMPILED_HEADER):PRECOMPILED_HEADER = $$IDE_SHARED_PATH/ctkcreator_gui_pch.h

