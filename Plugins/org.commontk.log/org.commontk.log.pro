include($$PWD/../../Plugins/Plugins.pri)
TARGET = log
DEFINES += HAVE_QT5

HEADERS += \
    ctkLogPlugin_p.h \
    ctkLogQDebug_p.h

SOURCES += \
    ctkLogPlugin.cpp \
    ctkLogQDebug.cpp

RESOURCES += \
    resource.qrc
