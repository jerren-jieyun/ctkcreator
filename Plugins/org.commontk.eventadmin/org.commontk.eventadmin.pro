include($$PWD/../../Plugins/Plugins.pri)
TARGET = eventadmin
DEFINES += HAVE_QT5

HEADERS += \
    adapter/ctkEAAbstractAdapter_p.h \
    adapter/ctkEAFrameworkEventAdapter_p.h \
    adapter/ctkEALogEventAdapter_p.h \
    adapter/ctkEAPluginEventAdapter_p.h \
    adapter/ctkEAServiceEventAdapter_p.h \
    dispatch/ctkEAChannel_p.h \
    dispatch/ctkEADefaultThreadPool_p.h \
    dispatch/ctkEAInterruptedException_p.h \
    dispatch/ctkEAInterruptibleThread_p.h \
    dispatch/ctkEALinkedQueue_p.h \
    dispatch/ctkEAPooledExecutor_p.h \
    dispatch/ctkEASignalPublisher_p.h \
    dispatch/ctkEASyncMasterThread_p.h \
    dispatch/ctkEAThreadFactory_p.h \
    dispatch/ctkEAThreadFactoryUser_p.h \
    handler/ctkEABlackList_p.h \
    handler/ctkEABlacklistingHandlerTasks_p.h \
    handler/ctkEACacheFilters_p.h \
    handler/ctkEACacheTopicHandlerFilters_p.h \
    handler/ctkEACleanBlackList_p.h \
    handler/ctkEAFilters_p.h \
    handler/ctkEAHandlerTasks_p.h \
    handler/ctkEASlotHandler_p.h \
    handler/ctkEATopicHandlerFilters_p.h \
    tasks/ctkEAAsyncDeliverTasks_p.h \
    tasks/ctkEADeliverTask_p.h \
    tasks/ctkEAHandlerTask_p.h \
    tasks/ctkEASyncDeliverTasks_p.h \
    tasks/ctkEASyncThread_p.h \
    util/ctkEABrokenBarrierException_p.h \
    util/ctkEACacheMap_p.h \
    util/ctkEACyclicBarrier_p.h \
    util/ctkEALeastRecentlyUsedCacheMap_p.h \
    util/ctkEALogTracker_p.h \
    util/ctkEARendezvous_p.h \
    util/ctkEATimeoutException_p.h \
    ctkEAConfiguration_p.h \
    ctkEAMetaTypeProvider_p.h \
    ctkEventAdminActivator_p.h \
    ctkEventAdminImpl_p.h \
    ctkEventAdminService_p.h

SOURCES += \
    adapter/ctkEAAbstractAdapter.cpp \
    adapter/ctkEAFrameworkEventAdapter.cpp \
    adapter/ctkEALogEventAdapter.cpp \
    adapter/ctkEAPluginEventAdapter.cpp \
    adapter/ctkEAServiceEventAdapter.cpp \
    dispatch/ctkEADefaultThreadPool.cpp \
    dispatch/ctkEAInterruptedException.cpp \
    dispatch/ctkEAInterruptibleThread.cpp \
    dispatch/ctkEALinkedQueue.cpp \
    dispatch/ctkEAPooledExecutor.cpp \
    dispatch/ctkEASignalPublisher.cpp \
    dispatch/ctkEASyncMasterThread.cpp \
    dispatch/ctkEAThreadFactoryUser.cpp \
    handler/ctkEACleanBlackList.cpp \
    handler/ctkEASlotHandler.cpp \
    tasks/ctkEASyncThread.cpp \
    util/ctkEABrokenBarrierException.cpp \
    util/ctkEACyclicBarrier.cpp \
    util/ctkEALogTracker.cpp \
    util/ctkEARendezvous.cpp \
    util/ctkEATimeoutException.cpp \
    ctkEAConfiguration.cpp \
    ctkEAMetaTypeProvider.cpp \
    ctkEventAdminActivator.cpp \
    ctkEventAdminService.cpp

RESOURCES += \
    resource.qrc
