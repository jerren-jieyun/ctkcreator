#-------------------------------------------------
#
# Project created by QtCreator 2018-03-07T11:07:21
#
#-------------------------------------------------
include($$PWD/../CTK.pri)
include($$IDE_LIB_PATH/ctk_dependencies.pri)

#version check qt
!minQtVersion(5, 4, 0) {
    message("Cannot build rp creator with Qt version $${QT_VERSION}.")
    error("Use at least Qt 5.4.0.")
}

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = APP
TEMPLATE = app

SOURCES += main.cpp

HEADERS  +=

# 主程序生成目录
DESTDIR += $$IDE_OUTPUT_APP_PATH
